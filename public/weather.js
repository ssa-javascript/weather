const API_KEY='c77a756a24cd6fe32d4ab53b52765a76';

const temp =document.querySelector("#temp");
const humidity =document.querySelector("#humidity");
const weatherMain =document.querySelector("#weather-main");
const weatherDesc =document.querySelector("#weather-desc");
const windSpeed =document.querySelector("#wind-speed");
const sysCountry =document.querySelector("#sys-country");
const cityName =document.querySelector("#city-name");
const clouds =document.querySelector("#clouds");
const weatherImg=document.querySelector('#weather-img');


const COORDS = 'coords';

function getWeather(lat,lon){
    fetch(
        `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${API_KEY}`
    )
    .then(function(response){
        return response.json();
    })
    .then(function(resp){
        temp.innerHTML="현재온도 : "+ (resp.main.temp- 273.15).toFixed(2);
        humidity.innerHTML="현재습도 : "+ resp.main.humidity;
        weatherMain.innerHTML="날씨 : "+ resp.weather[0].main;
        weatherDesc.innerHTML="상세날씨설명 : "+ resp.weather[0].description;
        windSpeed.innerHTML="바람   : "+ resp.wind.speed;
        sysCountry.innerHTML="나라   : "+ resp.sys.country;
        cityName.innerHTML="도시이름  : "+ resp.name;
        clouds.innerHTML="구름  : "+ (resp.clouds.all) +"%";
        weatherImg.src="http://openweathermap.org/img/w/" + resp.weather[0].icon + ".png"
    });
}
function saveCoords(coordsObj){
    localStorage.setItem(COORDS,JSON.stringify(coordsObj));    
} 

function handleGeoSucces(position) {
    console.log(position);
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;
    const coordsObj = {
        latitude,
        longitude
    };
    saveCoords(coordsObj);
    getWeather(latitude,longitude);
}

function handleGeoError() {
    console.log('Cant access geo location');
}

function askForCoords() {
    navigator.geolocation.getCurrentPosition(success => {
        handleGeoSucces(success);
      }, failure => {
        if (failure.message.startsWith("Only secure origins are allowed")) {
            handleGeoError()
        }
      });
}

function loadCoords() {
    const loadedCoords=null;
    if (loadedCoords === null) {
        askForCoords();
    } else {
        const parsedCoords=JSON.parse(loadedCoords);
        getWeather(parsedCoords.latitude,parsedCoords.longitude);
    }
}

function init(){
    loadCoords();
}

init();